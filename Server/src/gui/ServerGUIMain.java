package gui;

import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
@SuppressWarnings("serial")
public class ServerGUIMain extends JFrame{
    private JLabel messages;
    private JList games;

    public ServerGUIMain() {
        super("Connect 4 Server");
        setLayout(new BorderLayout());
        setBounds(0, 0, 400, 400);
        
        //MenuBar
        
        //List of games
        games = new JList();
        
        add(games, BorderLayout.CENTER);
        
        //general window stuff
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            //To allow for custom close operation
        addWindowListener(new WindowListener() {
            public void windowActivated(WindowEvent arg0) {
            }
            public void windowClosed(WindowEvent arg0) {
            }
            public void windowClosing(WindowEvent arg0) {
                close();
            }
            public void windowDeactivated(WindowEvent arg0) {
            }
            public void windowDeiconified(WindowEvent arg0) {
            }
            public void windowIconified(WindowEvent arg0) {
            }
            public void windowOpened(WindowEvent arg0) {
            }
        });
        setVisible(true);
    }
    
    private void close() {
        //TODO close gracefully
        System.exit(0);
    }
    
    public void setMessage(String message) {
        this.messages.setText(message);
    }
}
