package comm;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import gui.ServerGUIMain;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
public class ServerMain extends Thread{
    private ServerGUIMain gui;
    private ArrayList<ClientWrapper> sockets;//Clients
    private SocketAcceptor sa;
    private int port;

    /**
     * @param args - command line arguments: currently none
     */
    public static void main(String[] args) {
        ServerMain sm = new ServerMain(Integer.parseInt(args[0]));
        sm.run();
    }
    
    public ServerMain(int port) {
        this.port = port;
        gui = new ServerGUIMain();
        sockets = new ArrayList<ClientWrapper>();
        sa = new SocketAcceptor(port, this);
        sa.run();
    }

    public void addConnection(ClientWrapper cw) {
        sockets.add(cw);
    }

    public void run() {
        while(true) {
            //TODO
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void gotMessage(String message, int id) {
        System.out.println(message);
        
    }
}
