package comm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This object contains all information and objects necessary for the
 * server to have for each client.
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
public class ClientWrapper extends Thread implements Runnable {
    private int id;
    private BufferedReader readFromClient;
    private PrintWriter writeToClient;
    private ServerMain parent;
    
    public ClientWrapper(Socket cs, ServerMain parent) {
        this.parent = parent;
        try {
            readFromClient = new BufferedReader(new InputStreamReader(cs.getInputStream()));
            writeToClient = new PrintWriter(cs.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public void run() {
        String inputLine = "";
        try {
            while((inputLine = readFromClient.readLine()) != null) {
                if(inputLine.equals("disconnect")) {
                    //TODO disconnect
                }
                parent.gotMessage(inputLine, id);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public int getID() {
        return id;
    }
    
    public void write(String message) {
        writeToClient.println(message);
    }
}
