package comm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The sole intention of this object is to run continuously
 * waiting for a client to connect, and feeding the
 * new connection to the ServerMain
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
public class SocketAcceptor extends Thread {
    private int port;
    private ServerMain parent;
    
    public SocketAcceptor(int port, ServerMain parent) {
        this.port = port;
        this.parent = parent;
    }
    
    /**
     * continuously accept client connections
     */
    public void run() {
        ServerSocket ss = null;
        Socket cs = null;//Client socket
        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true) {
            try {
                cs = ss.accept();
                ClientWrapper cw = new ClientWrapper(cs, parent);
                cw.run();
                parent.addConnection(cw);
            } catch (IOException e) {
                System.err.println("Accept failed");
                e.printStackTrace();
            }
        }
    }
}
