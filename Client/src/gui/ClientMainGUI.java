package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import comm.ClientSocket;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
@SuppressWarnings("serial")
public class ClientMainGUI extends JFrame{
    private JLabel messages;
    private ClientSocket socket;
    
    public ClientMainGUI(ClientSocket s) {
        super("Connect 4");//JFrame title
        socket = s;
        setLayout(new BorderLayout());
        setBounds(100, 50, 800, 600);
        
        //MenuBar
        JMenuBar menuBar = new ClientMenuBar();
        setJMenuBar(menuBar);
        
        //Game panel
        JPanel gamePanel = new GamePanel();
        JButton b = new JButton("Button");
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                socket.sendMessage("button pressed");
            }
        });
        gamePanel.add(b);
        add(gamePanel, BorderLayout.WEST);
        
        
        //Chat panel
        JPanel chatPanel = new ChatPanel();
        add(chatPanel, BorderLayout.EAST);
        
        //Alert/status messages
        messages = new JLabel();
        
        //general stuff for this window
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            //Do nothing so as not to interfere with custom close operation
        addWindowListener(new WindowListener() {//anonymous window listener
            public void windowActivated(WindowEvent arg0) {
            }
            public void windowClosed(WindowEvent arg0) {
            }
            public void windowClosing(WindowEvent arg0) {
                close();
            }
            public void windowDeactivated(WindowEvent arg0) {
            }
            public void windowDeiconified(WindowEvent arg0) {
            }
            public void windowIconified(WindowEvent arg0) {
            }
            public void windowOpened(WindowEvent arg0) {
            }
        });
        
        setVisible(true);
    }
    
    private void close() {
        //TODO close gracefully
        System.exit(0);
    }
    
    public void setMessage(String message) {
        this.messages.setText(message);
    }
}
