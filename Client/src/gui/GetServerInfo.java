package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import comm.ClientMain;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
@SuppressWarnings("serial")
public class GetServerInfo extends JFrame{
    private String ip;
    private int port;
    private JTextField ipField;
    private JTextField portField;
    private GetServerInfo me;
    private ClientMain parent;
    
    public GetServerInfo(ClientMain p) {
        super("Enter server information");
        me = this;
        this.parent = p;
        setLayout(new GridLayout(5, 1));
        
        add(new JLabel("Enter server IP address:"));
        ipField = new JTextField();
        add(ipField);
        add(new JLabel("Enter server port"));
        portField = new JTextField();
        add(portField);
        JButton b = new JButton("Enter");
        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                ip = ipField.getText();
                try {
                    port = Integer.parseInt(portField.getText());
                } catch(NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Port must be a number", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                parent.setIP(ip);
                parent.setPort(port);
                me.dispose();
            }
        });
        add(b);
        setVisible(true);
    }
}
