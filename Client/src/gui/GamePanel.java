package gui;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel {
    //TODO panel which will hold the game board
    public GamePanel() {
        super();
        add(new JLabel("This is where the\ngame will go"));
    }
}
