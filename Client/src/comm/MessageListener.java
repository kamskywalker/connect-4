package comm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
public class MessageListener extends Thread implements Runnable{
    private BufferedReader readFromServer;
    private ClientSocket client;
    
    public MessageListener(ClientSocket s) throws IOException {
        readFromServer = new BufferedReader(new InputStreamReader(s.getInputStream()));
        client = s;
        start();
    }
    
    public void run() {
        while(true) {
            try {
                String message = readFromServer.readLine();
                client.serverMessage(message);
                sleep(500);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
        }
    }
}
