package comm;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import gui.ClientMainGUI;
import gui.GetServerInfo;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
public class ClientMain {
    private static ClientSocket socket;
    private String ip;
    private int port;

    /**
     * @param args - Command line arguments (currently unused)
     */
    public static void main(String[] args) {
        new ClientMain();
    }
    
    public ClientMain() {
        ip = "";
        port = 0;
        getServerInfo();
        //TODO initialize client
        while(port == 0) {
            try {
                Thread.sleep(1000);
                System.out.println("Sleeping 1 second");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            System.out.println("1");
            socket = new ClientSocket(ip, port);
            System.out.println("2");
            new ClientMainGUI(socket);
        } catch (IOException e) {
            System.out.println("Failed to connect");
            e.printStackTrace();
        }
    }
    
    private void getServerInfo() {
        new GetServerInfo(this);
    }

    public void setIP(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
