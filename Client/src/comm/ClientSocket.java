package comm;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author Kevin Mulligan, kam9115@rit.edu
 *
 */
public class ClientSocket extends Socket {
    private PrintWriter writeToServer;
    
    public ClientSocket(String server, int port) throws IOException {
        super(server, port);
        writeToServer = new PrintWriter(this.getOutputStream(), true);
        new MessageListener(this);
        }
    
    public void serverMessage(String message) {
        System.out.println("received message: " + message);
    }
    
    public void sendMessage(String message) {
        System.out.println("sent message: " + message);
        writeToServer.println(message);
    }
}
